﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using GrabGlavbuh.Models;
using HtmlAgilityPack;

namespace GrabGlavbuh
{
    class Program
    {
        private GrabContext _db;
        private string Auth = "fu4fh5spjtihywewm4cti3es";

        static void Main(string[] args)
        {
            var p = new Program();

            using (var db = new GrabContext())
            {
                p._db = db;
                p.Main();
            }

            Console.WriteLine("OK");
            Console.ReadKey();
        }

        private void Main()
        {
            // Запускать методы надо по очереди, там очень много
            //GrabMenu();
            //Clean();
            GrabDocuments();
        }

        private void Clean()
        {
            _db.Database.ExecuteSqlCommand("DELETE FROM [Pictures]");
            _db.Database.ExecuteSqlCommand("DELETE FROM [Blanks]");
            _db.Database.ExecuteSqlCommand("DELETE FROM [Documents]");
        }

        private void GrabMenu()
        {
            var queue = new Queue<MenuQueueItem>();
            queue.Enqueue(new MenuQueueItem() {Url = "https://www.1kadry.ru/system/content/rubricator/7/74/6353/"});

            var keysSet = new HashSet<string>();

            while (queue.Any())
            {
                var item = queue.Dequeue();

                var web = new HtmlWeb();
                HtmlDocument doc = web.Load(item.Url);
                HtmlNode rootEl = doc.DocumentNode.SelectSingleNode("//div[@class='b-scroller__container']");
                Node parent = null;
                int added = 0;
                foreach (HtmlNode el in rootEl.ChildNodes)
                {
                    if (el.Name == "h3" || el.Name == "h2")
                    {
                        parent = new Node()
                        {
                            Parent = item.Parent,
                            Name = el.ChildNodes[0].InnerText
                        };
                        added += 1;
                        _db.Nodes.Add(parent);
                    }
                    else if (el.Name == "ul")
                    {
                        foreach (HtmlNode liEl in el.ChildNodes)
                        {
                            var key = liEl.FirstChild.Attributes["href"].Value;

                            // Откуда дубли в меню? Действительно некоторые пункты ведут на одни и теже документы. Это не ошибка
                            /*
                            if (keysSet.Contains(key))
                            {
                                var dub = _db.Nodes.Local.First(x => x.Key == key);
                            }
                            */
                            keysSet.Add(key);

                            var node = new Node()
                            {
                                Parent = parent ?? item.Parent,
                                Key = key,
                                Name = liEl.FirstChild.FirstChild.InnerText,
                                IsLeaf = key.Contains("document")
                            };
                            added += 1;
                            _db.Nodes.Add(node);

                            if (!node.IsLeaf)
                            {
                                // Из /#/rubric/7/74/482 надо сделать https://www.1kadry.ru/system/content/subrubricator/7/74/794/
                                var nextUrl = "https://www.1kadry.ru/system/content/subrubricator/" +
                                              key.Replace("/#/rubric/", string.Empty) + "/";

                                queue.Enqueue(new MenuQueueItem() { Url = nextUrl, Parent = node });
                            }
                        }
                    }
                    else if (el.Name == "h1")
                    {
                    }
                    else
                        throw new Exception();
                }

                if (added == 0)
                    throw new Exception();
            }

            _db.SaveChanges();
            Console.WriteLine("Menu nodes = " + _db.Nodes.Count());
        }

        private void GrabDocuments()
        {
            var web = new HtmlWeb();
            var nodes = _db.Nodes.Where(x => x.IsLeaf).ToList();
            foreach (Node node in nodes)
            {
                if (_db.Documents.Any(x => x.Node.Id == node.Id))
                    continue;

                Console.WriteLine("Обрабатываем документ " + node.Id);

                // Преобразуем /#/document/118/9294/ в https://www.1kadry.ru/system/content/doc/118/23914/
                var url = "https://www.1kadry.ru/system/content/doc/" + node.Key.Replace("/#/document/", string.Empty);
                web.UseCookies = true;
                web.PreRequest =
                    request =>
                    {
                        request.CookieContainer.Add(new Cookie("ASP.NET_SessionId", Auth, "/", ".1kadry.ru"));
                        return true;
                    };

                HtmlDocument html = web.Load(url);

                if (html.DocumentNode.OuterHtml.Contains("компьютера зашел на сайт"))
                    throw new Exception("logout");

                if (html.DocumentNode.OuterHtml.Contains("Документ в платном доступе"))
                    throw new Exception("want money");

                var doc = new Document
                {
                    Node = node,
                    Html = html.DocumentNode.OuterHtml
                };
                _db.Documents.Add(doc);

                // Ищем файлы
                var docRefs = html.DocumentNode.SelectNodes("//a[contains(@class, 'js-export')]");
                var fileUrlsHash = new HashSet<string>();
                if (docRefs != null)
                {
                    foreach (HtmlNode docRef in docRefs)
                    {
                        if (!docRef.Attributes.Contains("href"))
                        {
                            Console.WriteLine("Не дают скачать");
                            continue;
                        }

                        var fileUrl = docRef.Attributes["href"].Value;
                        if (fileUrlsHash.Contains(fileUrl))
                            continue;

                        fileUrlsHash.Add(fileUrl);

                        var blank = new Blank();
                        blank.Document = doc;
                        blank.Title = docRef.InnerText;
                        blank.Key = fileUrl;

                        // Скачиваем
                        try
                        {
                            var file = DownloadFile("https://www.1kadry.ru" + fileUrl);
                            blank.Data = file.Data;
                            blank.FileName = file.FileName;
                            blank.IsEmpty = blank.Title.Contains("пустой бланк");
                        }
                        catch (WebException ex) when (ex.Message.Contains("404"))
                        {
                            Console.WriteLine("Файл не найден " + fileUrl);
                            continue;
                        }

                        _db.Blanks.Add(blank);
                    }
                }
                Console.WriteLine("Файлов " + fileUrlsHash.Count);

                // Ищем картинки
                var imgRefs = html.DocumentNode.SelectNodes("//a[@class='lightbox forms-img']");
                var pics = 0;
                if (imgRefs != null)
                {
                    foreach (HtmlNode imgRef in imgRefs)
                    {
                        var picture = new Picture();
                        picture.Document = doc;
                        picture.Key = imgRef.Attributes["href"].Value;

                        var r = DownloadPicture("https://www.1kadry.ru" + picture.Key);
                        picture.ContentType = r.ContentType;
                        picture.Data = r.Data;

                        _db.Pictures.Add(picture);
                    }
                }
                Console.WriteLine("Картинок " + pics);

                _db.SaveChanges();
                
            }
        }

        private DownloadFileResult DownloadFile(string url)
        {
            var result = new DownloadFileResult();

            using (var wc = new WebClient())
            {
                wc.Headers.Add(HttpRequestHeader.Cookie, "ASP.NET_SessionId=" + Auth);

                result.Data = wc.DownloadData(url);

                string header = wc.ResponseHeaders["Content-Disposition"] ?? string.Empty;
                const string filename = "filename=";
                int index = header.LastIndexOf(filename, StringComparison.OrdinalIgnoreCase);
                if (index > -1)
                {
                    var rawName = header.Substring(index + filename.Length).Trim('"');
                    result.FileName = Encoding.UTF8.GetString(Encoding.GetEncoding("iso-8859-1").GetBytes(rawName));
                }
                else
                    throw new Exception();
            }

            return result;
        }

        private DownloadPictureResult DownloadPicture(string url)
        {
            var result = new DownloadPictureResult();

            using (var wc = new WebClient())
            {
                wc.Headers.Add(HttpRequestHeader.Cookie, "ASP.NET_SessionId=" + Auth);

                result.Data = wc.DownloadData(url);
                result.ContentType = wc.ResponseHeaders["Content-Type"];
            }

            return result;
        }
    }

    class DownloadFileResult
    {
        public string FileName { get; set; }
        public byte[] Data { get; set; }
    }

    class DownloadPictureResult
    {
        public string ContentType { get; set; }
        public byte[] Data { get; set; }
    }

    class MenuQueueItem
    {
        public string Url { get; set; }
        public Node Parent { get; set; }
    }
}
