﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrabGlavbuh.Models
{
    public class Blank
    {
        public int Id { get; set; }

        [Required]
        public Document Document { get; set; }

        [Required, StringLength(50)]
        public string Key { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string FileName { get; set; }

        [Required]
        public bool IsEmpty { get; set; }

        [Required]
        public byte[] Data { get; set; }
    }
}
