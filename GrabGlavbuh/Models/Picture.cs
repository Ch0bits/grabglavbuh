﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrabGlavbuh.Models
{
    public class Picture
    {
        public int Id { get; set; }

        [Required, StringLength(40)]
        public string Key { get; set; }

        [Required]
        public Document Document { get; set; }
        
        [Required]
        public byte[] Data { get; set; }

        [Required, StringLength(15)]
        public string ContentType { get; set; }
    }
}
