﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrabGlavbuh.Models
{
    public class GrabContext : DbContext
    {
        public GrabContext():base("Main")
        {
            //this.Database.Log = Console.WriteLine;
        }

        public DbSet<Node> Nodes { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<Blank> Blanks { get; set; }
        public DbSet<Picture> Pictures { get; set; }
    }
}
