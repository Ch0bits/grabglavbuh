﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrabGlavbuh.Models
{
    public class Document
    {
        public int Id { get; set; }

        [Required]
        public Node Node { get; set; }

        [Required]
        public string Html { get; set; }
    }
}
