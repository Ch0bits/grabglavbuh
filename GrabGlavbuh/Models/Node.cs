﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrabGlavbuh.Models
{
    public class Node
    {
        public int Id { get; set; }

        public Node Parent { get; set; }

        [StringLength(30), Index]
        public string Key { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public bool IsLeaf { get; set; }
    }
}
