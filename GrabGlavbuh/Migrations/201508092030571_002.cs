namespace GrabGlavbuh.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _002 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Blanks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Key = c.String(nullable: false),
                        Title = c.String(nullable: false),
                        FileName = c.String(nullable: false),
                        Data = c.Binary(nullable: false),
                        Document_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Documents", t => t.Document_Id, cascadeDelete: true)
                .Index(t => t.Document_Id);
            
            CreateTable(
                "dbo.Documents",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Html = c.String(nullable: false),
                        Node_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Nodes", t => t.Node_Id, cascadeDelete: true)
                .Index(t => t.Node_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Blanks", "Document_Id", "dbo.Documents");
            DropForeignKey("dbo.Documents", "Node_Id", "dbo.Nodes");
            DropIndex("dbo.Documents", new[] { "Node_Id" });
            DropIndex("dbo.Blanks", new[] { "Document_Id" });
            DropTable("dbo.Documents");
            DropTable("dbo.Blanks");
        }
    }
}
