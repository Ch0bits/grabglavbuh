namespace GrabGlavbuh.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _004 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Pictures",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Key = c.String(nullable: false, maxLength: 40),
                        Data = c.Binary(nullable: false),
                        ContentType = c.String(nullable: false, maxLength: 15),
                        Document_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Documents", t => t.Document_Id, cascadeDelete: true)
                .Index(t => t.Document_Id);
            
            AlterColumn("dbo.Blanks", "Key", c => c.String(nullable: false, maxLength: 50));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Pictures", "Document_Id", "dbo.Documents");
            DropIndex("dbo.Pictures", new[] { "Document_Id" });
            AlterColumn("dbo.Blanks", "Key", c => c.String(nullable: false));
            DropTable("dbo.Pictures");
        }
    }
}
