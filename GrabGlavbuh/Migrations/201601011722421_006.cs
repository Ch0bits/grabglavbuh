namespace GrabGlavbuh.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _006 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Blanks", "Extension");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Blanks", "Extension", c => c.String(nullable: false, maxLength: 5));
        }
    }
}
