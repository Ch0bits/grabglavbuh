namespace GrabGlavbuh.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _003 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Blanks", "IsEmpty", c => c.Boolean(nullable: false));
            AddColumn("dbo.Blanks", "Extension", c => c.String(nullable: false, maxLength: 5));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Blanks", "Extension");
            DropColumn("dbo.Blanks", "IsEmpty");
        }
    }
}
