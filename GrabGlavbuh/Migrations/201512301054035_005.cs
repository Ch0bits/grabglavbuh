namespace GrabGlavbuh.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _005 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Nodes", new[] { "IsLeaf" });
        }
        
        public override void Down()
        {
            CreateIndex("dbo.Nodes", "IsLeaf", unique: true);
        }
    }
}
