namespace GrabGlavbuh.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _001 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Nodes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Key = c.String(maxLength: 30),
                        Name = c.String(nullable: false),
                        IsLeaf = c.Boolean(nullable: false),
                        Parent_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Nodes", t => t.Parent_Id)
                .Index(t => t.Key)
                .Index(t => t.IsLeaf, unique: true)
                .Index(t => t.Parent_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Nodes", "Parent_Id", "dbo.Nodes");
            DropIndex("dbo.Nodes", new[] { "Parent_Id" });
            DropIndex("dbo.Nodes", new[] { "IsLeaf" });
            DropIndex("dbo.Nodes", new[] { "Key" });
            DropTable("dbo.Nodes");
        }
    }
}
